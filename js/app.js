$(document).ready(function(){
  // smooth scrolling
  $('nav a[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top - 120
        }, 1000);
        return false;
      }
    }
  });
  $('.partner-contents nav ul li a').click( function() {
    $('.partner-contents nav ul li a').removeClass('active');
    $(this).addClass('active');
  });
  // stop auto scrolling
  $('.carousel').carousel('pause');
});
